var express = require('express');
const docRoute = require('./doc')

var router = express.Router();

router.use('/doc', docRoute);

router.get('/', function(req, res, next) {
  res.json({});
});

module.exports = router;
